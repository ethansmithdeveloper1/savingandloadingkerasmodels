# Import Dependancies
from keras.models import load_model
from keras.datasets import mnist
from keras.utils import to_categorical
import matplotlib.pyplot as plt

# Load Model
model = load_model('model.h5')

# Show Summarization
summary = model.summary()
print(summary)

# Load Data Set
(train_data, train_labels), (test_data_raw, test_labels) = mnist.load_data()

# Reshape to 28 x 28 x 1 --- 1 = Black and White
train_data = train_data.reshape(60000, 28, 28, 1)
test_data = test_data_raw.reshape(10000, 28, 28, 1)

# Create Categorical Labels
train_labels = to_categorical(train_labels)
test_labels = to_categorical(test_labels)

#Predict on Model
index = int(input("Index of Test?"))
prediction = model.predict([[test_data[index]]])
actual = test_labels[index]

print("Prediction : " + str(prediction) + " - Actual : " + str(actual))

plt.figure()
plt.gray()
ax = plt.subplot(111)
plt.imshow(test_data_raw[index])
plt.show()


# python3 ./node_modules/keras-js/python/encoder.py