from keras.models import Sequential
from keras.layers import Dense, Conv2D, Flatten, MaxPool2D, Dropout
import matplotlib.pyplot as plt
from keras.datasets import mnist
from keras.utils import to_categorical

# Download and Seperate Data
(train_data, train_labels), (test_data_raw, test_labels) = mnist.load_data()

# Reshape to 28 x 28
train_data = train_data.reshape(60000, 28, 28, 1)
test_data = test_data_raw.reshape(10000, 28, 28, 1)

# Create Categorical Labels
train_labels = to_categorical(train_labels)
test_labels = to_categorical(test_labels)

# Instantiate Model
model = Sequential()

# Configure Layers
model.add(Conv2D(32, kernel_size=(3, 3),
                 activation='relu', input_shape=(28, 28, 1)))
model.add(MaxPool2D(pool_size=(2, 2)))
model.add(Dropout(.25))

model.add(Conv2D(64, kernel_size=(3, 3), activation='relu'))
model.add(MaxPool2D(pool_size=(2, 2)))
model.add(Dropout(.25))

model.add(Conv2D(64, kernel_size=(3, 3), activation='relu'))
model.add(MaxPool2D(pool_size=(2, 2)))
model.add(Dropout(.25))

model.add(Flatten())
model.add(Dense(1000, activation='relu'))
model.add(Dropout(.3))
model.add(Dense(10, activation='softmax'))

# Compile for Accuracy
model.compile(optimizer='adam', loss='categorical_crossentropy',
              metrics=['accuracy'])

# Train Model
model.fit(train_data, train_labels, validation_data=(
    test_data, test_labels), epochs=4)

def get_answer(pred_filter):
    largest = 0
    index = 0
    for i in range(len(pred_filter)):
        p = pred_filter[i]
        if(p > largest):
            largest = p
            index = i

    return index

# Predict first Images
num_images = 50
predictions = model.predict(test_data[:num_images])

plt.figure()
plt.gray()

# Demonstrate Sample Images
for i in range(num_images):
    ax = plt.subplot(5, 10, i + 1)
    plot_label = "P : " + str(get_answer(predictions[i])) + " - A : " + str(get_answer(test_labels[i]))
    ax.title.set_text(plot_label)
    plt.imshow(test_data_raw[i])
plt.show()


#Save Model?
save_response = input("Save Model? (y/n)")
if(save_response == "y"):
    model.save("model.h5")
    print("Model Saved.")